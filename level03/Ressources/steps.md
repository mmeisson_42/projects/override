## level03

We have here a program that is a pain to decrypt. By using a tool like retdec, we could understand that this program take in input a number, and that this number is decrypted.

There is some values that are decrypted "as is", basically 1..21 - 10..16 when substracted by a "magic" value, 0x1337d00d (322424845) .

From reading the assembly code we know that a substraction is done between the input passed as password and a number hardcoded in the main() and passed to test().

```bash
0x1337d00d = 322424845
```

So we are just going to try these values and see what's going on

```bash

for i in 1 2 3 4 5 6 7 8 9 16 17 18 19 20 21
do
    echo $((322424845 - $i)) > /tmp/03
    cat /tmp/03 - | ./level03
done

...
***********************************
*		level03		**
***********************************
Password:
Invalid Password

***********************************
*		level03		**
***********************************
Password:
Invalid Password

***********************************
*		level03		**
***********************************

$> cd ../level
$> cat .pass
    kgv3tkEb9h2mLkRsPkXRfc2mHbjMxQzvb2FrgKkf
```

And to know what is the good password for those who wonder :

```bash
$> echo $((322424845 - $(cat /tmp/03)))
18
```
