#include <stdio.h>
#include <stdlib.h>

void    clear_stdin(void)
{
	int c = 0;

	while (c != '\n' && c != -1) // EOF
	{
		c = getchar();
	}
}

unsigned int get_unum(void)
{
	unsigned int n;

	n = 0; // +6
	fflush(stdout); // +21
	scanf("%u", &n); // + 41
	clear_stdin();
	return (n);
}

int store_number(int *tab)
{
	int    n;
	unsigned int    i;

	n = 0;
	i = 0;
	printf(" Number: ");
	n = get_unum();
	printf(" Index: ");
	i = get_unum();

	if ((i / 3) * 3 == i || (n >> 0x18) == 0xb7) // (i / 3) * 3 == i -> same as i % 3
	{
		puts(" *** ERROR! ***");
		puts("   This index is reserved for wil!");
		puts(" *** ERROR! ***");
		return (1);
	}
	printf(" Number at data[%u] is %u\n", i, tab[i]);
	return (0);
}

int	main(int ac, char **av, char **env)
{
	int     n;
	char    buf[20];
	int     nb[100] = {0};

	n = 0;
	bzero(buf, 5);
	while (*av)
	{
		memset(&av, 0, strlen(*av));
		av++;
	}
	while (*env)
	{
		memset(&env, 0, strlen(*env));
		env++;
	}
	puts(
		"----------------------------------------------------\n"
		" Welcome to wil's crappy number storage service!\n"   
		"\n----------------------------------------------------\n"
		" Commands:                                "
	);
	while (1) // +384
	{
		printf("Input command: "); //+308
		n = 1;
		fgets(buf, 20, stdin);
		buf[strlen(buf - 1)] = 0;

		if (!strncmp(buf, "store", 5))
			n = store_number(nb); // +455
		else if (!strncmp(buf, "read", 4))
			n = read_number(nb); // +520
		else if (!strncmp(buf, "quit", 4)) // +557
			return (0);
		if (n != 0)
			printf(" Failed to do %s command\n", &buf); //+607
		else
			printf(" Completed %s command successfully\n", &buf);
		bzero(&buf, 5);
	}
	return (0);
}
