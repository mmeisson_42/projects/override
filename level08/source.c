#include <stdio.h>

void	log_wrapper(FILE *log_f, char *str, char *file)
{
	char buf[255];

	strcpy(buf, str);
	snprintf(buf + strlen(buf), 254 - strlen(buf), file);
	buf[strcspn(buf, "\n")] = '\0';
	fprintf(log_f, "LOG: %s\n", buf);
}

int main(int ac, char **av)
{
	FILE *log_f;
	FILE *file;
	char buf[100];
	char c;
	int		fd;

	if (ac != 2)
		printf("Usage: %s filename\n", av[0]);		
	else
	{
		log_f = fopen("./backups/.log", "w");
		if (!log_f)
		{
			printf("ERROR: Failed to open %s\n", "./backups/.log");
			exit(1);
		}
		log_wrapper(log_f, "Starting back up: ", av[1]);
		file = fopen(av[1], "r");
		if (!file)
		{
			printf("ERROR: Failed to open %s\n", av[1]);
			exit(1);
		}
		strcpy(buf, "./backups/");
		strncat(buf, av[1], 99 - strlen(av[1]));
		fd = open(buf, 0xc1, 0x1b0); // +424
		if (!fd)
		{
			printf("ERROR: Failed to open %s%s\n", "./backups/", av[1]);
			exit(1);
		}
		while(c != EOF)
		{
			write(fd, &c, 1);
			c = fgetc(file);
		}
		log_wrapper(log_f, "Finished back up ", av[1]);
		fclose(file);
		close(fd);
	}
	return (0);
}
