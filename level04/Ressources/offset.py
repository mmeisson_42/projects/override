#! /usr/bin/env python

l = []
for letter in "acdefghijklmnopqrstuvwxyz":
    for number in range(9):
        l.append("%s%s" % (letter, number))

print("".join(l))
