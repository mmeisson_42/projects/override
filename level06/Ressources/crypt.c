#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

int auth(char *login);

int auth(char *login)
{
    unsigned int crypt;  // ebp - 0x10
    size_t index;        // ebp - 0x14
    size_t login_length; // ebp - 0xc == esp - 0x1c
    int value;
    // esp == ebp + 0x28

    login_length = strnlen(login, ' ');
    if (login_length < 4)
    {
        return 1;
    }

    crypt = ((unsigned int)*(login + 3) ^ 0x1337) + 0x5eeded;
    index = 0;

    while (index < login_length)
    {
        value = (unsigned int)*(login + index);

        if (value < 0x1f)
        {
            return 1;
        }
        crypt += ((value ^ crypt) % 1337);
        index++;
    }
    return crypt;
}

int main(
    __attribute__((unused)) int argc,
    char **argv)
{
    printf("%d\n", auth(argv[1]));
    return 0;
}