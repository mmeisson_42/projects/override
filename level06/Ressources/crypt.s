section .text

global auth

auth:
    push   ebp
    mov    ebp,esp
    sub    esp,0x28
    mov    DWORD [esp+0x4],0x8048a63
 
    mov    eax,DWORD [ebp+0x8]
    mov    DWORD [esp],eax

    mov    eax,DWORD [ebp+0x8]
    add    eax,0x3
    movzx  eax,BYTE [eax]
    movsx  eax,al
    xor    eax,0x1337
    add    eax,0x5eeded
    mov    DWORD [ebp-0x10],eax
    mov    DWORD [ebp-0x14],0x0
    jmp    loop_cond
_loop_start:
    mov    eax,DWORD [ebp-0x14]
    add    eax,DWORD [ebp+0x8]
    movzx  eax,BYTE [eax]
    cmp    al,0x1f
    jg     _l1
    mov    eax,0x1
    jmp    bye
_l1:
    mov    eax,DWORD [ebp-0x14]
    add    eax,DWORD [ebp+0x8]
    movzx  eax,BYTE [eax]
    movsx  eax,al
    mov    ecx,eax
    xor    ecx,DWORD [ebp-0x10]
    mov    edx,0x88233b2b
    mov    eax,ecx
    mul    edx
    mov    eax,ecx
    sub    eax,edx
    shr    eax,1
    add    eax,edx
    shr    eax,0xa
    imul   eax,eax,0x539
    mov    edx,ecx
    sub    edx,eax
    mov    eax,edx
    add    DWORD [ebp-0x10],eax
    add    DWORD [ebp-0x14],0x1
loop_cond:
    mov    eax,DWORD [ebp-0x14]
    cmp    eax,DWORD [ebp-0xc]
    jl     _loop_start

    mov    eax,DWORD [ebp-0x10]
bye:
    leave  
    ret  
