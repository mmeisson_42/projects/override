## level06

We have here a binary that tries to "login" people  
There login means start a shell with user level07

There is no exploit like an overflow of any kind in this level, the exercice is to understand how to get a password associated with a login.

There is a basic check in the binary based on a ptrace(TRACEME) but that is easily bypassed, and since the required password is computed and stored on the stack, we just have to let the program run until he is doing the comparison between the given and the required one.

Since that this exploit is not interesting, we tried to make a program that compute a password from a given login.

We first tried to take the disass from gdb and build a function in asm from it, but the result is not the good one for any reason.

We finished by complete properly the source file which is written in C. This does the job.

```bash
$> gdb ./level06
    disas auth
        ...
            0x080487b5 <+109>:	call   0x80485f0 <ptrace@plt>
            0x080487ba <+114>:	cmp    eax,0xffffffff
            0x080487bd <+117>:	jne    0x80487ed <auth+165>
            ...
            0x080487e3 <+155>:	mov    eax,0x1
            0x080487e8 <+160>:	jmp    0x8048877 <auth+303>
            ...
            0x08048866 <+286>:	cmp    eax,DWORD PTR [ebp-0x10]
            0x08048869 <+289>:	je     0x8048872 <auth+298>
    b *auth+114
    b *auth+286
    run
        Starting program: /home/users/level06/level06 
        ***********************************
        *		level06		  *
        ***********************************
        -> Enter Login: abcdefgh
        ***********************************
        ***** NEW ACCOUNT DETECTED ********
        ***********************************
        -> Enter Serial: 503
    set $eax=1
    c
    x/x $ebp-0x10
        6234502
```

```bash
$> ./level06
    ***********************************
    *		level06		  *
    ***********************************
    -> Enter Login: abcdefgh
    ***********************************
    ***** NEW ACCOUNT DETECTED ********
    ***********************************
    -> Enter Serial: 6234502
    Authenticated!
$> cd ../level07
$> cat .pass
    GbcPDRgsFK77LNnnuh7QyFYA2942Gp8yKj9KrWD8
```

Bonus :

```bash
$> cd Ressources
$> cc -m32 crypt.c
$> ./a.out pikachu
    6233768

# Back in the vm
$> ./level06
    ***********************************
    *		level06		  *
    ***********************************
    -> Enter Login: pikachu
    ***********************************
    ***** NEW ACCOUNT DETECTED ********
    ***********************************
    -> Enter Serial: 6233768
    Authenticated!
$> cd ../level07
$> cat .pass
    GbcPDRgsFK77LNnnuh7QyFYA2942Gp8yKj9KrWD8
