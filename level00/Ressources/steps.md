## level00

By testing the executable file we can see that a password is expected to unlock something we do not know yet.

By going through the asm source code of the main() function we can see that a comparaison is being made right after a call to scanf() and if this comparaison evaluates to true it will execute a system() function spawning a /bin/sh shell.
By analysing the registers value we can see that scanf() expects an int ("%d").

Now that we have all those elements in hands, it is pretty clear that we just have to enter 5276 as password in order to spwan a shell.

```bash
$> ./level00
    ***********************************
    * 	     -Level00 -		  *
    ***********************************
    Password:5276

    Authenticated!
$> cd ../level01
$> cat .pass
    uSq2ehEGT6c9S24zbshexZQBXUGrncxn5sD5QfGL
```
